package main;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.HBox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private TextField txtUrl;
    @FXML
    private Button btnSend;
    @FXML
    private ChoiceBox<String> chMethod;
    @FXML
    private TextArea txtResponse;
    @FXML
    private HBox hbBody;

    private TextArea txtBody;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        txtBody = new TextArea();
        txtBody.setWrapText(true);
        txtBody.setPrefWidth(578);
        txtBody.setPrefHeight(250);

        chMethod.getItems().add("GET");
        chMethod.getItems().add("POST");
        chMethod.getItems().add("PUT");
        chMethod.getItems().add("DELETE");
        chMethod.getSelectionModel().select(0);

        chMethod.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            hbBody.getChildren().clear();
            if (newValue.equals(1) || newValue.equals(2)) {
                hbBody.getChildren().add(txtBody);
            }
        });

        txtUrl.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                sendData();
            }
        });

        txtResponse.setWrapText(true);
    }

    @FXML
    private void sendData() {

        try {
            switch (chMethod.getValue()) {
                case "GET":
                    sendGETRequest();
                    break;
                case "DELETE":
                    sendDELETERequest();
                    break;
                case "PUT":
                    sendPUTRequest();
                    break;
                case "POST":
                    sendPOSTRequest();
                    break;
                default:
                    txtResponse.setText("Invalid Request Method");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendPOSTRequest() throws IOException {
        HttpURLConnection connection = getConnection();
        sendBody(connection);

        checkResponseCode(connection, HttpURLConnection.HTTP_CREATED);

        connection.getInputStream();
        txtResponse.setText(connection.getResponseMessage());
        connection.disconnect();
    }

    private void sendPUTRequest() throws IOException {
        HttpURLConnection connection = getConnection();
        sendBody(connection);

        checkResponseCode(connection, 200);

        connection.getInputStream();
        txtResponse.setText(connection.getResponseMessage());
        connection.disconnect();
    }

    private void sendDELETERequest() throws IOException {
        HttpURLConnection connection = getConnection();

        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.connect();

        checkResponseCode(connection, 200);

        txtResponse.setText(connection.getResponseMessage());
        connection.disconnect();
    }

    private void sendGETRequest() throws IOException {
        URL url = new URL(txtUrl.getText());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        checkResponseCode(connection, 200);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

        String response;
        while ((response = bufferedReader.readLine()) != null) {
            txtResponse.setText(formatResponse(response));
        }

        connection.disconnect();
    }

    private String formatResponse(String response) {
        StringBuilder formattedResponse = new StringBuilder();
        for (int i = 0; i < response.length(); i++) {

            char c = response.charAt(i);
            Character next = i != (response.length() - 1) ? response.charAt(i + 1) : null;

            formattedResponse.append(c);

            if (c == '[' || c == '{' || c == '}' || c == ',' || (c == '"' && next == '}')) {
                formattedResponse.append("\n");
            }
        }

        return formattedResponse.toString();
    }

    private HttpURLConnection getConnection() throws IOException {
        URL url = new URL(txtUrl.getText());
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod(chMethod.getValue());

        return connection;
    }

    private void sendBody(HttpURLConnection connection) throws IOException {
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write(txtBody.getText());
        out.close();
    }

    private void checkResponseCode(HttpURLConnection connection, int code) throws IOException {
        if (connection.getResponseCode() != code) {
            throw new RuntimeException("Failed: HTTP error code: " + connection.getResponseCode());
        }
    }
}
